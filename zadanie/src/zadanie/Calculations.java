package zadanie;

public class Calculations {

	private int numberOfItem;
	private int net;
	private int percentTax;
	
	public int getNumberofitem() 
	{
		return numberOfItem;
	}
	public int getNet() 
	{
		return net;
	}
	public int getPercenttax() 
	{
		return percentTax;
	}
	public void setNumberofitem(int numberofitem) 
	{
		this.numberOfItem = numberofitem;
	}
	public void setNet(int net) 
	{
		this.net = net;
	}
	public void setPercenttax(int percenttax) 
	{
		this.percentTax = percenttax;
	}
	
	int gross()
	{
		int gross=net*percentTax/100;
		gross = gross + net;
		return gross;
	}
	int totalnet()
	{
		int totalnet=net*numberOfItem;
		return totalnet;
	}
	int totalgross()
	{
		int totalgross=numberOfItem*gross();
		return totalgross;
	}
}
