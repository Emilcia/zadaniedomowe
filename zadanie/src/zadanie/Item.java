package zadanie;

public class Item {

	private String name;
	private String unit;
	private Calculations calc;
	
	public String getName()
	{
		return name;
	}
	public String getUnit()
	{
		return unit;
	}
	public Calculations getCalc() 
	{
		return calc;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setUnit(String unit)
	{
		this.unit=unit;
	}
	public void setCalc(Calculations calc) 
	{
		this.calc = calc;
	}
	

}
